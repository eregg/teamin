package cc.teamin.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 *
 * Teamin 主配置文件类
 *
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
@Configuration
@ConfigurationProperties(prefix = "teamin.core")
public class TeaminProperties {

    private CmsPropertis cms = new CmsPropertis();

    private WeixinProperties wx = new WeixinProperties();

    public CmsPropertis getCms() {
        return cms;
    }

    public void setCms(CmsPropertis cms) {
        this.cms = cms;
    }

    public WeixinProperties getWx() {
        return wx;
    }

    public void setWx(WeixinProperties wx) {
        this.wx = wx;
    }
}
