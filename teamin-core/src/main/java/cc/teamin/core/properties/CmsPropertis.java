package cc.teamin.core.properties;

/**
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public class CmsPropertis {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
