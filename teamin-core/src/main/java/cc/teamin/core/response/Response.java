package cc.teamin.core.response;

/**
 *
 * 响应给前端的封装对象
 *
 * Created by e on 2017/11/29
 *
 */
public class Response {

    public Response(ResponseEnum responseEnum){
        this(responseEnum, null);
    }

    public Response(ResponseEnum responseEnum, Object body){
        this.setCode(responseEnum.getCode());
        this.setMessage(responseEnum.getMessage());
        this.setBody(body);
    }
    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误信息
     */
    private String message;

    /**
     * 数据信息
     */
    private Object body;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }
}
