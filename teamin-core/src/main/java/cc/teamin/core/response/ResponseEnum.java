package cc.teamin.core.response;

/**
 *
 * 返回状态Enum
 *
 * Created by e on 2017/11/29
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public enum ResponseEnum {

    SYS_ERROR(-1, "failed"),
    SYS_OK(1000, "success");


    ResponseEnum(Integer code, String message){
        this.setCode(code);
        this.setMessage(message);
    }

    /**
     * 状态吗
     */
    private Integer code;
    /**
     * 状态信息
     */
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
