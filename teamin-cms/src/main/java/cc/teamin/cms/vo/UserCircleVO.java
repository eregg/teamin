package cc.teamin.cms.vo;

import java.sql.Timestamp;

/**
 * Created by e on 2017/12/6
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public class UserCircleVO {

    private String circleName;
    private Timestamp joinTime;

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public Timestamp getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Timestamp joinTime) {
        this.joinTime = joinTime;
    }
}
