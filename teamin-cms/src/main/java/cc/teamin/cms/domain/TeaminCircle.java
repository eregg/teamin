package cc.teamin.cms.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 *
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
@Entity
@Table(name = "tb_circle")
public class TeaminCircle {

    @Id @GeneratedValue
    private Integer circleId;//int(11) NOT NULL AUTO_INCREMENT COMMENT '圈子ID',
    private String circleName;//varchar(45) DEFAULT NULL COMMENT '圈子名称',
    private String descript;//varchar(200) DEFAULT NULL COMMENT '圈子描述!',
    private String img;//varchar(45) DEFAULT NULL COMMENT '圈子图标',
    private Timestamp createTime;//timestamp(1) NULL DEFAULT CURRENT_TIMESTAMP(1) COMMENT '圈子创建时间',
    private Integer createrId;//int(11) DEFAULT NULL COMMENT '圈子的创建者',
    private Integer isdelete;//int(1) DEFAULT '0',
    private Timestamp updateTime;//timestamp(1) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(1),
    private Integer canMemberInvite;//smallint(6) DEFAULT '0' COMMENT '普通成员能邀请成员 1表示可以邀请,0表示不可邀请',
    private Integer canMemberShare;//smallint(6) DEFAULT '0' COMMENT '普通成员能分享 1表示可以,0:不可以',
    private Integer openGroupCircleCheck;//smallint(3) DEFAULT '0' COMMENT '只能从指定群的来源,1表示开打,0表示关闭',
    private Integer memberCount; //成员数量
    private String userName; //创建人名称


    public Integer getCircleId() {
        return circleId;
    }

    public void setCircleId(Integer circleId) {
        this.circleId = circleId;
    }

    public String getCircleName() {
        return circleName;
    }

    public void setCircleName(String circleName) {
        this.circleName = circleName;
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Integer getCreaterId() {
        return createrId;
    }

    public void setCreaterId(Integer createrId) {
        this.createrId = createrId;
    }

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCanMemberInvite() {
        return canMemberInvite;
    }

    public void setCanMemberInvite(Integer canMemberInvite) {
        this.canMemberInvite = canMemberInvite;
    }

    public Integer getCanMemberShare() {
        return canMemberShare;
    }

    public void setCanMemberShare(Integer canMemberShare) {
        this.canMemberShare = canMemberShare;
    }

    public Integer getOpenGroupCircleCheck() {
        return openGroupCircleCheck;
    }

    public void setOpenGroupCircleCheck(Integer openGroupCircleCheck) {
        this.openGroupCircleCheck = openGroupCircleCheck;
    }

    public Integer getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(Integer memberCount) {
        this.memberCount = memberCount;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
