package cc.teamin.cms.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 *
 * 用户实体
 *
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
@Entity
@Table(name = "tb_user")
public class TeaminUser {

    @Id @GeneratedValue
    private Integer userId;//int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
    private String userName;//varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户名',
    private String headIcon;//varchar(400) DEFAULT NULL COMMENT '头像',
    private Timestamp createTime;//timestamp(1) NULL DEFAULT CURRENT_TIMESTAMP(1) COMMENT '创建时间',
    private String phone;//varchar(20) DEFAULT NULL COMMENT '手机号码',
    private String email;//varchar(45) DEFAULT NULL COMMENT '邮箱',
    private String company;//varchar(45) DEFAULT NULL COMMENT '所属公司',
    private Timestamp lastLoginTime;//datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,

    private Timestamp joinTime;
    private Timestamp updateTime; //
    private String openId;//微信openId


    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHeadIcon() {
        return headIcon;
    }

    public void setHeadIcon(String headIcon) {
        this.headIcon = headIcon;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Timestamp getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Timestamp lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Timestamp getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Timestamp joinTime) {
        this.joinTime = joinTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
}
