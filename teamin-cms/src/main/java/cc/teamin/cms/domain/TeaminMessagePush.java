package cc.teamin.cms.domain;

import java.sql.Timestamp;

/**
 * 消息推送 实体类
 * Created by e on 2017/12/7
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public class TeaminMessagePush {

    private Long pushId;        //bigint(20) NOT NULL AUTO_INCREMENT COMMENT '推送id',
    private String pushContent;        //varchar(256) NOT NULL DEFAULT '' COMMENT '推送内容',
    private Integer pushType;        //int(10) NOT NULL DEFAULT '0' COMMENT '推送类型0全部',
    private Long pushUserId;        //bigint(20) NOT NULL DEFAULT '-1' COMMENT '推送用户id',
    private Integer arrived;        //int(10) NOT NULL DEFAULT '0' COMMENT '抵达数量',
    private Integer haveRead;        //int(10) NOT NULL DEFAULT '0' COMMENT '已读数量',
    private Timestamp createTime;        //datetime DEFAULT CURRENT_TIMESTAMP COMMENT '发送时间',

    public Long getPushId() {
        return pushId;
    }

    public void setPushId(Long pushId) {
        this.pushId = pushId;
    }

    public String getPushContent() {
        return pushContent;
    }

    public void setPushContent(String pushContent) {
        this.pushContent = pushContent;
    }

    public Integer getPushType() {
        return pushType;
    }

    public void setPushType(Integer pushType) {
        this.pushType = pushType;
    }

    public Long getPushUserId() {
        return pushUserId;
    }

    public void setPushUserId(Long pushUserId) {
        this.pushUserId = pushUserId;
    }

    public Integer getArrived() {
        return arrived;
    }

    public void setArrived(Integer arrived) {
        this.arrived = arrived;
    }

    public Integer getHaveRead() {
        return haveRead;
    }

    public void setHaveRead(Integer haveRead) {
        this.haveRead = haveRead;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
}
