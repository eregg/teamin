package cc.teamin.cms.repository;

import cc.teamin.cms.domain.TeaminUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public interface TeaminUserRepository extends JpaRepository<TeaminUser, Integer> {

//    //List<TeaminUser>
//    @Query(name = "select * from tb_user", nativeQuery = true)
//    List<TeaminUser> listAll();
}
