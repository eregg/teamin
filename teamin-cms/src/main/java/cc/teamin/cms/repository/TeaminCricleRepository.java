package cc.teamin.cms.repository;

import cc.teamin.cms.domain.TeaminCircle;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public interface TeaminCricleRepository extends JpaRepository<TeaminCircle, Integer> {

}
