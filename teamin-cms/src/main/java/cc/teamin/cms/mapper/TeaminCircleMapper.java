package cc.teamin.cms.mapper;

import cc.teamin.cms.domain.TeaminCircle;

import java.util.List;

/**
 *
 * 圈子Mapper
 *
 * Created by e on 2017/12/6
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public interface TeaminCircleMapper {


    /**
     * 获取圈子列表 根据圈子名称，状态
     * @param teaminCircle
     * @return
     */
    List<TeaminCircle> findByLikeNameAndStatus(TeaminCircle teaminCircle);

    /**
     * 更新圈子
     * @param teaminCircle
     * @return
     */
    Integer update(TeaminCircle teaminCircle);

    /**
     * 删除圈子 根据圈子id
     * @param circleId
     * @return
     */
    Integer delete(Integer circleId);

}
