package cc.teamin.cms.mapper;

import cc.teamin.cms.domain.TeaminMessagePush;
import cc.teamin.cms.domain.TeaminUser;
import cc.teamin.cms.vo.UserCircleVO;

import java.util.List;

/**
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public interface TeaminPushMapper {

    /**
     * 插入消息推送
     * @param teaminMessagePush
     * @return
     */
    Integer insert(TeaminMessagePush teaminMessagePush);


    /**
     * 获取所有消息
     * @return
     */
    List<TeaminMessagePush> findAll();
}
