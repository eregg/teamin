package cc.teamin.cms.mapper;

import cc.teamin.cms.domain.TeaminUser;
import cc.teamin.cms.vo.UserCircleVO;

import java.util.List;

/**
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public interface TeaminUserMapper{

    /**
     * 获取 圈子成员列表
     * @param circleId
     * @return
     */
    List<TeaminUser> findByCircleId(Integer circleId);

    /**
     * 获取 用户加入的圈子列表
     * @param userId
     * @return
     */
    List<UserCircleVO> findCircleByUserId(Integer userId);
}
