package cc.teamin.cms.controller;

import cc.teamin.cms.domain.TeaminMessagePush;
import cc.teamin.cms.service.ITeaminMessagePushService;
import cc.teamin.core.response.Response;
import cc.teamin.core.response.ResponseEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 *
 * 推送控制器
 *
 * Created by e on 2017/12/7
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
@RestController
public class TeaminPushController {

    @Autowired
    private ITeaminMessagePushService pushService;

    /**
     * 获取推送消息列表
     * @return
     */
    @GetMapping("/push/push.html")
    public ModelAndView toPush(){

        return new ModelAndView("push/push");
    }

    /**
     * 获取推送消息列表
     * @return
     */
    @GetMapping("/push/messages")
    public Object findAllMessage(){

        List<TeaminMessagePush> messages = this.pushService.findAll();

        return messages;
    }

    /**
     * 推送消息
     * @return
     */
    @PostMapping("/push/messages")
    public ResponseEntity<Response> pushMessage(TeaminMessagePush teaminMessagePush){

        teaminMessagePush.setPushUserId(1000L);

        Response response = this.pushService.sendMessage(teaminMessagePush);

        return ResponseEntity.ok(new Response(ResponseEnum.SYS_OK, response));
    }
}
