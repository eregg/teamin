package cc.teamin.cms.controller;

import cc.teamin.cms.domain.TeaminCircle;
import cc.teamin.cms.domain.TeaminUser;
import cc.teamin.cms.service.ITeaminCricleService;
import cc.teamin.cms.service.ITeaminUserService;
import cc.teamin.cms.vo.UserCircleVO;
import cc.teamin.core.response.Response;
import cc.teamin.core.response.ResponseEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by e on 2017/12/6
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
@RestController
public class CircleController {

    @Autowired
    private ITeaminCricleService teaminCricleService;

    @Autowired
    private ITeaminUserService teaminUserService;


    /**********Page begin***********/

    /**
     * 圈子列表
     * @param model
     * @return
     */
    @GetMapping("/circle/circles.html")
    public ModelAndView circles(Model model,
                                @RequestParam(name = "circleName", required = false) String circleName,
                                @RequestParam(name = "isdelete", required = false) Integer isdelete){

        TeaminCircle teaminCircle = new TeaminCircle();
        teaminCircle.setCircleName(circleName);
        teaminCircle.setIsdelete(isdelete);

        List<TeaminCircle> teaminCircles = this.teaminCricleService.findByLikeNameAndStatus(teaminCircle);

        model.addAttribute("list", teaminCircles);
        model.addAttribute("query", teaminCircle);

        return new ModelAndView("circle/circles", "circleModel", model);
    }


    /**
     * 圈子中的成员列表
     * @param model
     * @param circleId
     * @return
     */
    @GetMapping("/circle/{circleId}/users.html")
    public ModelAndView findByCircleId(Model model,
                                       @PathVariable(name = "circleId") Integer circleId){

        model.addAttribute("circleId", circleId);

        return new ModelAndView("circle/circleUsers", "circleModel", model);
    }


    /**
     * 用户加入的圈子列表
     * @param model
     * @param userId
     * @return
     */
    @GetMapping("/circle/user/{userId}/circles.html")
    public ModelAndView findCircleByUserId(Model model,
                                       @PathVariable(name = "userId") Integer userId){
        model.addAttribute("userId", userId);

        return new ModelAndView("circle/userCircles", "circleModel", model);
    }
    /**********Page end***********/


    /**********REST begin***********/

    /**
     * 圈子列表
     * @param circleName
     * @param status
     * @return
     */
    @GetMapping("/circle/circles")
    public Object listCircles(
            @RequestParam(name = "circleName", required = false) String circleName,
            @RequestParam(name = "status", required = false) Integer status){

        TeaminCircle teaminCircle = new TeaminCircle();
        teaminCircle.setCircleName(circleName);
        teaminCircle.setIsdelete(status);

        List<TeaminCircle> teaminCircles = this.teaminCricleService.findByLikeNameAndStatus(teaminCircle);

        return teaminCircles;
    }

    /**
     * 圈子中的成员列表
     * @param circleId
     * @return
     */
    @GetMapping("/circle/{circleId}/users")
    public Object listByCircleId(@PathVariable(name = "circleId") Integer circleId){

        List<TeaminUser> teaminUsers = this.teaminUserService.findByCircleId(circleId);


        return teaminUsers;
    }


    /**
     * 用户加入的圈子列表
     * @param
     * @param userId
     * @return
     */
    @GetMapping("/circle/user/{userId}/circles")
    public Object listCircleByUserId(@PathVariable(name = "userId") Integer userId){

        List<UserCircleVO> userCircleVOS = this.teaminUserService.findCircleByUserId(userId);


        return userCircleVOS;
    }

    /**
     * 恢复圈子
     * @param circleId
     * @return
     */
    @PostMapping("/circle/{circleId}/valid")
    public ResponseEntity<Response> valid(@PathVariable(name = "circleId") Integer circleId){
        Response response = this.teaminCricleService.updateValid(circleId);

        return ResponseEntity.ok(response);
    }

    /**
     * 删除圈子 逻辑删除
     * @param circleId
     * @return
     */
    @PostMapping("/circle/{circleId}/invalid")
    public ResponseEntity<Response> invalidCircle(@PathVariable(name = "circleId") Integer circleId){
        Response response = this.teaminCricleService.updateInvalid(circleId);

        return ResponseEntity.ok(response);
    }

    /**
     * 删除圈子 物理删除
     * @param circleId
     * @return
     */
    @PostMapping("/circle/{circleId}/delete")
    public ResponseEntity<Response> deleteCircle(@PathVariable(name = "circleId") Integer circleId){
        Response response = this.teaminCricleService.delete(circleId);

        return ResponseEntity.ok(response);
    }



}
