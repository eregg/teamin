package cc.teamin.cms.controller;

import cc.teamin.core.properties.TeaminProperties;
import cc.teamin.core.response.Response;
import cc.teamin.core.response.ResponseEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
@RestController
public class IndexController {

    @Autowired
    private TeaminProperties teaminProperties;

    @GetMapping("/")
    public ResponseEntity<Response> index(){

        return ResponseEntity.ok(new Response(ResponseEnum.SYS_OK, teaminProperties.getCms().getName()));
    }

    @GetMapping("/toIndex")
    public ModelAndView toIndex(Model model){
        System.out.print("test");
        model.addAttribute("username", "eregg");

        return new ModelAndView("index", "userModel", model);
    }

    @GetMapping("/dashboard")
    public ModelAndView dashboard(Model model){

        return new ModelAndView("dashboard/index", "userModel", model);
    }


}
