package cc.teamin.cms.websocket;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {


    @MessageMapping("/hello")
    @SendTo("/topic/message")
    public ResponseMessage greeting(RequestMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new ResponseMessage("Hello, " + message.getContent() + "!");
    }

}
