package cc.teamin.cms.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * web Socket 组件
 *
 * Created by e on 2017/12/8
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
@Component
public class WebSocketComponent {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    /**
     * 发送消息
     * @param messageJson json 字符串
     */
    public void sendMessage(String messageJson) {

        messagingTemplate.convertAndSend("/topic/message", messageJson);
    }
}
