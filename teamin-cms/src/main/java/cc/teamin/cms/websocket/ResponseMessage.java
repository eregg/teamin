package cc.teamin.cms.websocket;

/**
 *
 * 服务器返回给浏览器的消息由这个类来承载：
 *
 * Created by e on 2017/12/7
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public class ResponseMessage {

    public ResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    private String responseMessage;

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }
}
