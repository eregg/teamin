package cc.teamin.cms.service.impl;

import cc.teamin.cms.domain.TeaminCircle;
import cc.teamin.cms.mapper.TeaminCircleMapper;
import cc.teamin.cms.service.ITeaminCricleService;
import cc.teamin.core.response.Response;
import cc.teamin.core.response.ResponseEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * 圈子服务类
 *
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
@Service
public class TeaminCricleServiceImpl implements ITeaminCricleService {

    @Autowired
    private TeaminCircleMapper teaminCircleMapper;

    @Override
    public List<TeaminCircle> findByLikeNameAndStatus(TeaminCircle teaminCircle) {
        return this.teaminCircleMapper.findByLikeNameAndStatus(teaminCircle);
    }

    @Override
    public Response updateValid(Integer circleId) {
        return updateStatus(circleId, 0);
    }

    @Override
    public Response updateInvalid(Integer circleId) {
        return updateStatus(circleId, 1);
    }

    @Override
    public Response delete(Integer circleId) {
        Response response = null;
        try {
            Integer res = this.teaminCircleMapper.delete(circleId);

            if(res > 0){
                response = new Response(ResponseEnum.SYS_OK);
            }else{
                response = new Response(ResponseEnum.SYS_ERROR);
            }
        }catch (Exception e){
            response = new Response(ResponseEnum.SYS_ERROR, e.getMessage());
        }
        return response;
    }

    private Response updateStatus(Integer circleId, Integer status){
        Response response = null;
        try {
            TeaminCircle teaminCircle = new TeaminCircle();
            teaminCircle.setCircleId(circleId);
            teaminCircle.setIsdelete(status);
            Integer res = this.teaminCircleMapper.update(teaminCircle);

            if(res > 0){
                response = new Response(ResponseEnum.SYS_OK);
            }else{
                response = new Response(ResponseEnum.SYS_ERROR);
            }
        }catch (Exception e){
            response = new Response(ResponseEnum.SYS_ERROR, e.getMessage());
        }
        return response;
    }
}
