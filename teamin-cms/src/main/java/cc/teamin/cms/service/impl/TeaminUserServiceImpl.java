package cc.teamin.cms.service.impl;

import cc.teamin.cms.domain.TeaminUser;
import cc.teamin.cms.mapper.TeaminUserMapper;
import cc.teamin.cms.service.ITeaminUserService;
import cc.teamin.cms.vo.UserCircleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * 用户服务类
 *
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
@Service
public class TeaminUserServiceImpl implements ITeaminUserService {


    @Autowired
    private TeaminUserMapper teaminUserMapper;


    @Override
    public List<TeaminUser> findByCircleId(Integer circleId) {
        return teaminUserMapper.findByCircleId(circleId);
    }

    @Override
    public List<UserCircleVO> findCircleByUserId(Integer userId) {
        return this.teaminUserMapper.findCircleByUserId(userId);
    }
}
