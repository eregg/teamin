package cc.teamin.cms.service;

import cc.teamin.cms.domain.TeaminMessagePush;
import cc.teamin.core.response.Response;

import java.util.List;

/**
 *
 * 消息推送服务接口
 *
 * Created by e on 2017/12/7
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public interface ITeaminMessagePushService {

    /**
     * 发送消息
     * @param messagePush
     * @return
     */
    Response sendMessage(TeaminMessagePush messagePush);

    /**
     * 获取所有已发送的消息列表
     * @return
     */
    List<TeaminMessagePush> findAll();
}
