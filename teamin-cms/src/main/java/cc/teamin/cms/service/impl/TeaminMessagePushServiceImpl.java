package cc.teamin.cms.service.impl;

import cc.teamin.cms.component.WeixinComponent;
import cc.teamin.cms.domain.TeaminMessagePush;
import cc.teamin.cms.mapper.TeaminPushMapper;
import cc.teamin.cms.service.ITeaminMessagePushService;
import cc.teamin.cms.websocket.ResponseMessage;
import cc.teamin.cms.websocket.WebSocketComponent;
import cc.teamin.core.response.Response;
import cc.teamin.core.response.ResponseEnum;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * 消息推送 服务类
 *
 * 1.公众号推送
 * 2.小程序推送
 *
 * Created by e on 2017/12/7
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
@Service
public class TeaminMessagePushServiceImpl implements ITeaminMessagePushService {

    @Autowired
    private TeaminPushMapper teaminPushMapper;


    @Autowired
    private WeixinComponent weixinComponent;

    @Autowired
    private WebSocketComponent webSocketComponent;

    @Override
    public Response sendMessage(TeaminMessagePush messagePush) {

        String messageJson = JSONObject.fromObject(new ResponseMessage(messagePush.getPushContent())).toString();
        //1.推送到微信公众号
        this.weixinComponent.sendMessageToAll(messageJson);
        //2.推送到小程序
        this.webSocketComponent.sendMessage(messageJson);
        //3.保存消息到数据库
        this.teaminPushMapper.insert(messagePush);

        return new Response(ResponseEnum.SYS_OK);
    }

    @Override
    public List<TeaminMessagePush> findAll() {
        return this.teaminPushMapper.findAll();
    }
}
