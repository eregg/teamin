package cc.teamin.cms.service;

import cc.teamin.cms.domain.TeaminCircle;
import cc.teamin.core.response.Response;

import java.util.List;

/**
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public interface ITeaminCricleService {


    /**
     * 获取圈子列表 根据圈子名称，状态
     * @param teaminCircle
     * @return
     */
    List<TeaminCircle> findByLikeNameAndStatus(TeaminCircle teaminCircle);

    /**
     * 使圈子有效 更新圈子状态
     * @param circleId
     * @return
     */
    Response updateValid(Integer circleId);

    /**
     * 使圈子无效 更新圈子状态
     * @param circleId
     * @return
     */
    Response updateInvalid(Integer circleId);

    /**
     * 删除圈子 根据圈子id
     * @param circleId
     * @return
     */
    Response delete(Integer circleId);

}
