package cc.teamin.cms.service;

import cc.teamin.cms.domain.TeaminUser;
import cc.teamin.cms.vo.UserCircleVO;

import java.util.List;

/**
 *
 * 用户服务接口
 *
 * Created by e on 2017/12/5
 *
 * @author <a href="http://eregg.com">eregg</a>
 */
public interface ITeaminUserService {


    /**
     * 获取加入圈子的用户列表
     * @param circleId
     * @return
     */
    List<TeaminUser> findByCircleId(Integer circleId);

    /**
     * 获取用户加入的圈子列表
     * @param userId
     * @return
     */
    List<UserCircleVO> findCircleByUserId(Integer userId);
}
