package cc.teamin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
@MapperScan("cc.teamin.*.mapper") //配置扫描mapper接口的地址
public class TeaminCmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeaminCmsApplication.class, args);
	}
}
